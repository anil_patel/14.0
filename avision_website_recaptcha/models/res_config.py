
from odoo import models, fields, api, _, SUPERUSER_ID


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    recaptcha_site_key = fields.Char(string='Recaptcha Site Key')
    recaptcha_secret_key = fields.Char(string="Recaptcha Secret Key")

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            recaptcha_site_key=self.env['ir.config_parameter'].sudo().get_param(
                'avision_website_recaptcha.recaptcha_site_key'),
            recaptcha_secret_key=self.env['ir.config_parameter'].sudo().get_param(
                'avision_website_recaptcha.recaptcha_secret_key'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('avision_website_recaptcha.recaptcha_site_key',
                                                         self.recaptcha_site_key)
        self.env['ir.config_parameter'].sudo().set_param('avision_website_recaptcha.recaptcha_secret_key',
                                                         self.recaptcha_secret_key)