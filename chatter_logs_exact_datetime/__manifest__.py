# -*- coding: utf-8 -*-

{
    'name': 'Chatter Logs Exact Datetime',
    'version': '1.0',
    'summary': """ This module show the exact date and time on odoo chatter logs
""",
    "author": "Valueble IT Solution",
    "support": "anil.patel13688@gmail.com",
    'website': 'valuebleitsolution.odoo.com',
    "description": """
        Chatter exact date and time
        logs exact date and time
        chatter extended
        show date time in chatter
        show exact date and time 
        logs exact date and time
        show logs real time.
    """,
    'license': 'AGPL-3',
    'company': 'Valueble IT Solution',
    'images': ['static/description/img_1.png'],
    'data': [
    ],
    'qweb' : [
        'static/src/xml/message.xml'
    ],
    'demo': [],
    "price": "20",
    "currency": "EUR",
    'installable': True,
    'application': True,
}
