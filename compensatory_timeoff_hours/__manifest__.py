# -*- coding: utf-8 -*-

{
    'name': 'Monthly Compensatory Timeoff',
    'version': '1.0',
    'summary': """
        This module allocate monthly compensatory hours to employee and employee can use in timeoff module.
    """,
    'category': 'Human Resources',
    "description": """
        Compensatory time off allocation.
        Monthly Compensatory timeoff.
        Timesheet overtime allocation.
        calculate overtime hours.
        calculate monthly overtime.
        overtime time off.
        allocate overtime in timeoff.
        employee overtime.
        employee overtime time off.
        monthly compensatory allocation.
        calculate actual working hours.
        timesheet compensatory allocation.
        overtime allocation.
    """,
    'price': "30",
    'website': 'valuebleitsolution.odoo.com',
    'currency': 'USD',
    'license': 'AGPL-3',
    'images': ['static/description/img_6.png'],
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    'depends': ['base', 'hr', 'hr_timesheet', 'project', 'hr_holidays'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/hr_employee.xml',
        'wizard/compensatory.xml',
        'views/analytic_line.xml'
    ],
    'demo': [],
    'installable': True,
    'application': True,
}
