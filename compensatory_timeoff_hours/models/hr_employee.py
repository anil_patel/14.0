# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
from datetime import datetime, date, timedelta
import calendar


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    target_hour = fields.Float(string="Target Hours", compute="get_target_hours")
    actual_working_hour = fields.Float(string="Actual Working Hours", compute="get_actual_working_hours")
    compensatory_hours = fields.Float(string="Compensatory Hours", compute="get_compensatory_hours")
    # annual_timeoff_id = fields.Many2one('hr.leave.type', string="Annual Paid Leave")
    compensatory_hours_type = fields.Many2one('hr.leave.type', string="Compensatory Hours Allocation")

    def get_compensatory_hours(self):
        for employee in self:
            employee.compensatory_hours = employee.actual_working_hour - employee.target_hour

    def get_calculated_hours(self, start_date):
        end_day = calendar.monthrange(start_date.year, start_date.month)[-1]
        start_date = start_date.replace(day=1)
        end_date = start_date.replace(day=end_day)
        total_hours = 0
        timesheet_ids = self.env['account.analytic.line'].sudo().search([
            ('employee_id', '=', self.id),
            ('date', '>=', start_date),
            ('date', '<=', end_date),
            ('company_id', '=', self.company_id.id)
        ])
        if timesheet_ids:
            total_hours += sum(timesheet_ids.mapped('unit_amount'))
        return total_hours

    def get_actual_working_hours(self):
        for employee in self:
            employee.actual_working_hour = 0
            employee.actual_working_hour = employee.get_calculated_hours(date.today())

    def monthly_workdays(self , start, end, excluded):
        days = 0
        while start <= end:
            if start.isoweekday() not in excluded:
                days += 1
            start += timedelta(days=1)
        return days

    def get_employee_total_working_hrs(self):
        for employee in self:
            total_hour = 0
            employee.target_hour = 0
            if employee.resource_calendar_id and employee.resource_calendar_id.attendance_ids:
                total_hour = 0
                for each in employee.resource_calendar_id.attendance_ids:
                    total_hour += each.hour_to - each.hour_from
            return total_hour

    def get_target_hours(self):
        for employee in self:
            employee.target_hour = 0
            start_date = date.today()
            end_day = calendar.monthrange(start_date.year, start_date.month)[-1]
            start_date = start_date.replace(day=1)
            end_date = start_date.replace(day=end_day)
            working_hour = employee.get_employee_total_working_hrs()
            working_day = 7
            excluded = []
            if employee.resource_calendar_id.weekend_off_days_ids:
                working_day = 7 - len(employee.resource_calendar_id.weekend_off_days_ids)
                for weekoff in employee.resource_calendar_id.weekend_off_days_ids:
                    excluded.append(weekoff.day_no)
            if working_hour > 0:
                working_hour = working_hour / working_day
            employee.target_hour = working_hour * employee.monthly_workdays(start_date, end_date, excluded)
