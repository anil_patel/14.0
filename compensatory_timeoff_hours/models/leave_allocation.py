# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools


class HrLeaveAllocation(models.Model):
    _inherit = 'hr.leave.allocation'

    allocation_month = fields.Integer()
    allocation_year = fields.Integer()