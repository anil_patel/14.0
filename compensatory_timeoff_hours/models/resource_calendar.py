# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools


class ResourceCalendar(models.Model):
    _inherit = 'resource.calendar'

    weekend_off_days_ids = fields.Many2many('weekend.off.days', string="Weekend Off Days")


class WeekendOffDays(models.Model):
    _name = 'weekend.off.days'
    _description = 'Weekend Off Days'

    name = fields.Char(strig="Name")
    day_no = fields.Integer(string="Day No.")