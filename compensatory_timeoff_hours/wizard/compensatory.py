# -*- coding: utf-8 -*-


# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
from datetime import datetime, date, timedelta
import calendar


class CompensatoryAllocation(models.TransientModel):
    _name = 'compensatory.allocation'
    _description = "Compensatory Allocation"

    date = fields.Date(string="Date", required=True)

    def create_allocations(self):
        employee_ids = self.env['hr.employee'].sudo().search([('id', 'in', self._context.get('active_ids'))])
        allocation_lst = []
        for employee in employee_ids:
            employee.target_hour = 0
            start_date = self.date
            end_day = calendar.monthrange(start_date.year, start_date.month)[-1]
            start_date = start_date.replace(day=1)
            end_date = start_date.replace(day=end_day)
            working_hour = employee.get_employee_total_working_hrs()
            working_days = 7
            excluded = []
            if employee.resource_calendar_id.weekend_off_days_ids:
                working_days = 7 - len(employee.resource_calendar_id.weekend_off_days_ids)
                for weekoff in employee.resource_calendar_id.weekend_off_days_ids:
                    excluded.append(weekoff.day_no)
            if working_hour > 0:
                working_hour = working_hour / working_days
            target_hour = working_hour * employee.monthly_workdays(start_date, end_date,excluded)
            actual_working_hour = employee.get_calculated_hours(start_date)
            compensatory_hours = actual_working_hour - target_hour
            if not employee.compensatory_hours_type:
                raise UserError(_('Please define Compensatory Hours Allocation in employee'))
            if compensatory_hours > 0:
                existing_allocation = self.env['hr.leave.allocation'].search([
                    ('employee_id', '=', employee.id),
                    ('holiday_status_id', '=', employee.compensatory_hours_type.id),
                    ('allocation_month', '=', start_date.month),
                    ('allocation_year', '=', start_date.year)
                ])
                if existing_allocation:
                    raise UserError(_('You have already allocate compensatory hour for employee ' + employee.name))
                allocation_id = self.env['hr.leave.allocation'].create({
                    'employee_id': employee.id,
                    'holiday_status_id': employee.compensatory_hours_type.id,
                    'allocation_month': start_date.month,
                    'allocation_year': start_date.year,
                    'number_of_days': compensatory_hours / working_hour,
                    'allocation_type': 'regular',
                    'name' : 'Monthly Compensatory Allocation',
                })
                allocation_id.number_of_hours_display = compensatory_hours
                allocation_id.action_approve()
