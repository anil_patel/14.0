# -*- coding: utf-8 -*-
{
    'name': "Elearning Extended",
    'summary': """changes in base functionality of E-learning""",
    'description': """
    In this module we change the website page of course.
User can see the Price of that course in the course page.
Also on the Course Page when we take the cursor on course it will open one Popup and then we can read the Information of that Course on that Popup.
Inside the Perticular course you can check the description of that Course and You also check What will you learn from this Course.
    """,
    'author': "Valueble IT Solution",
    'website': 'valuebleitsolution.odoo.com',
    'version': '13.0.1.0',
    'images': [],
    'license': 'AGPL-3',
    'price': "40",
    'currency': 'USD',
    'depends': ['website','website_slides','website_profile','website_sale'],
    'data': [
        'security/ir.model.access.csv',
        'data/elearning_categories.xml',
        'wizard/slide_channel_invite_views.xml',
        'views/res_users_view.xml',
        'views/slide_channel_views.xml',
        'views/res_config_settings_inherit_view.xml',
        'views/elearnig_template.xml'
    ],
    'qweb': ['static/src/xml/web.xml'],
    'images': ['static/description/main_screenshot.png'],
    "application": False,
    'installable': True,

}
