from odoo import http, _
from odoo.http import request
from odoo.addons.website.controllers.main import Website
from odoo.addons.website_slides.controllers.main import WebsiteSlides


class CustomerRegistration(http.Controller):

    @http.route('/course', type='json', auth='public', website=True)
    def course(self, **kw):
        value = {}
        course_id = request.env['slide.channel'].sudo().search([('name', '=', kw.get('course_name'))], limit=1)
        # if course_id.product_id:
        #     return course_id.product_id.id
        value['data'] = request.env['ir.ui.view'].render_template("elearning_extended.elerning_detail", {
            'course': course_id,
            'course_name': course_id.name,
            'course_time': kw.get('course_time'),
            'product_id': course_id.product_id
            # 'suggested_products': order._cart_accessories()
        })
        # print("\n \norder-------------", order)
        # value['website_sale.short_cart_summary'] = request.env['ir.ui.view'].render_template(
        #     "website_sale.short_cart_summary", {
        #         'website_sale_order': order,
        #     })
        return value

class Website(Website):

    def data_call(self, **post):
        print('\n\n===call method')
        domain = request.website.website_domain()
        channels_all = request.env['slide.channel'].search(domain)
        print('\n\n\n=======', channels_all, domain)
        # if not request.env.user._is_public():
        channels_my = channels_all.filtered(lambda channel: channel.is_member).sorted('completion', reverse=True)[:3]
        # else:
        #     channels_my = request.env['slide.channel']
        print('\n\n\n==========channels_my', channels_my)
        channels_popular = channels_all.sorted('total_votes', reverse=True)[:3]
        channels_newest = channels_all.sorted('create_date', reverse=True)[:3]

        achievements = request.env['gamification.badge.user'].sudo().search([('badge_id.is_published', '=', True)],
                                                                            limit=5)
        if request.env.user._is_public():
            challenges = None
            challenges_done = None
        else:
            challenges = request.env['gamification.challenge'].sudo().search([
                ('category', '=', 'slides'),
                ('reward_id.is_published', '=', True)
            ], order='id asc', limit=5)
            challenges_done = request.env['gamification.badge.user'].sudo().search([
                ('challenge_id', 'in', challenges.ids),
                ('user_id', '=', request.env.user.id),
                ('badge_id.is_published', '=', True)
            ]).mapped('challenge_id')

        users = request.env['res.users'].sudo().search([
            ('karma', '>', 0),
            ('website_published', '=', True)], limit=5, order='karma desc')

        # values = WebsiteSlides._prepare_user_values(**post)
        values = {}
        values.update({
            'channels_my': channels_my,
            'channels_popular': channels_popular,
            'channels_newest': channels_newest,
            'achievements': achievements,
            'users': users,
            # 'top3_users': WebsiteSlides._get_top3_users(),
            'challenges': challenges,
            'challenges_done': challenges_done,
        })

        return values

    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        print('\n\n my controller call')
        homepage = request.website.homepage_id
        result = self.data_call(**kw)
        if homepage and (
                homepage.sudo().is_visible or request.env.user.has_group('base.group_user')) and homepage.url != '/':
            print('\n\n=====call1111')
            return request.env['ir.http'].reroute(homepage.url)

        website_page = request.env['ir.http']._serve_page()
        print('\n\n\n website page', website_page.qcontext)
        if website_page:
            print('\n\n=====call222222')
            website_page.qcontext['data'] = result
            website_page.qcontext['user'] = request.env.user
            return website_page
        else:
            print('\n\n=====call33333')
            top_menu = request.website.menu_id
            first_menu = top_menu and top_menu.child_id and top_menu.child_id.filtered(lambda menu: menu.is_visible)
            if first_menu and first_menu[0].url not in ('/', '', '#') and (
            not (first_menu[0].url.startswith(('/?', '/#', ' ')))):
                return request.redirect(first_menu[0].url)
        print('\n\n=====call44444444')
        raise request.not_found()