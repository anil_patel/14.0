# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import uuid

from odoo import api, fields, models, tools, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression

class Channel(models.Model):
    """ A channel is a container of slides. """
    _inherit = 'slide.channel'

    
    lst_price = fields.Float('Public Price')
    public_categ_ids = fields.Many2many('product.public.category', relation='product_public_category_slide_channel_rel', string='Website Product Category',
                                        help="The product will be available in each mentioned e-commerce category. Go to"
                                        "Shop > Customize and enable 'E-commerce categories' to view all e-commerce categories.")

    course_requirement = fields.Html('Course Requirement')
    course_lern = fields.Html('Learn Course')

    @api.onchange('enroll','lst_price','public_categ_ids')
    def onchange_enroll(self):
        if self.enroll == 'payment':
            if not self.product_id:
                categ_id = self.env.ref('product.product_category_1', raise_if_not_found=False)
                product = self.env['product.product'].create({
                            'name' : self.name,
                            'lst_price' : self.lst_price,
                            'sale_ok' : True,
                            'purchase_ok' : False,
                            'type' : 'service',
                            'categ_id' : categ_id.id if categ_id else False
                    })
                if product:
                    self.write({'product_id': product.id})
            
            if self.product_id and self.lst_price:
                self.product_id.write({'lst_price': self.lst_price})
            
            if self.product_id and self.public_categ_ids:
                self.product_id.write({'public_categ_ids': [(6,0, self.public_categ_ids.ids)]})
                
    