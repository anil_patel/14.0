# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import re

from email.utils import formataddr

from odoo import api, fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

emails_split = re.compile(r"[;,\n\r]+")


class SlideChannelInvite(models.TransientModel):
    _inherit = 'slide.channel.invite'
    
    
    res_users_categ_id      = fields.Many2one('res.users.categ','Users Category')
    
    @api.onchange('res_users_categ_id')
    def onchange_res_users_categ_id(self):
        if not self.res_users_categ_id:
            self.partner_ids = False
            
        if self.res_users_categ_id:
            users = self.env['res.users'].search([('res_users_categ_id','=',self.res_users_categ_id.id)])
            partner_list = []
            if users:
                for user in users:
                    partner_list.append(user.partner_id.id)
            
            if partner_list:
                self.partner_ids = [(6,0, partner_list)]
            else:
                self.partner_ids = False
    