# -*- coding: utf-8 -*-

{
    'name': 'Fixed Shipping Cost Weight Range',
    'version': '1.0',
    'summary': """This module allow to configure fixed shipping cost based on the order weight and configuration.
""",
    "description": """
    fixed shipping cost.
    delivery charges based on weight.
    delivery cost based on configuration.
    fixed delivery cost based on weight.
    fixed shipping cost range.
    shipping cost based on order weight.
    shipping cost weight.
    fixed shipping charges.
    fixed shipping charges based on weight.
    website shipping cost.
    order shipping cost.
    fixed shipping cost configuration.
    fixed shipping charges configuration.
    allow to configure fixed shipping based on order weight. 
""",
    'website': 'valuebleitsolution.odoo.com',
    'author': 'Anil(anil.patel13688@gmail.com)',
    'depends': ['base', 'sale', 'delivery', 'website_sale', 'website_sale_delivery'],
    'price': "40",
    'currency': 'USD',
    'license': 'AGPL-3',
    'images': ['static/description/img_2.png'],
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    'data': [
        'security/ir.model.access.csv',
        'view/delivery_carrier.xml',
        'view/delivery_template.xml'
    ],
    'demo': [],
    'installable': True,
    'application': True,
}
