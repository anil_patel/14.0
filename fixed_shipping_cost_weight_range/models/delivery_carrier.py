# -*- coding: utf-8 -*-
import logging
import psycopg2

from odoo import api, fields, models, registry, SUPERUSER_ID, _

_logger = logging.getLogger(__name__)


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    use_rang_config = fields.Boolean(string="Use Fixed Shipping Cost Weight Range")
    shipping_cost_range_ids = fields.One2many('fixed.shipping.cost.range', 'delivery_carrier_id')

    def fixed_rate_shipment(self, order):
        carrier = self._match_address(order.partner_shipping_id)
        if not carrier:
            return {'success': False,
                    'price': 0.0,
                    'error_message': _('Error: this delivery method is not available for this address.'),
                    'warning_message': False}
        if self.use_rang_config:
            # price = order.pricelist_id.get_product_price(self.product_id, 1.0, order.partner_id)
            weight = sum(
                [(line.product_id.weight * line.product_uom_qty) for line in order.order_line if not line.is_delivery])
            price = 0
            for weight_range in self.shipping_cost_range_ids:
                if weight >= weight_range.weight_start_range and weight <= weight_range.weight_end_range:
                    price = weight_range.price
                    break
            company = self.company_id or order.company_id or self.env.company
            if company.currency_id and company.currency_id != order.currency_id:
                price = company.currency_id._convert(price, order.currency_id, company, fields.Date.today())
            return {'success': True,
                    'price': price,
                    'error_message': False,
                    'warning_message': False}
        else:
            price = order.pricelist_id.get_product_price(self.product_id, 1.0, order.partner_id)
            company = self.company_id or order.company_id or self.env.company
            if company.currency_id and company.currency_id != order.currency_id:
                price = company.currency_id._convert(price, order.currency_id, company, fields.Date.today())
            return {'success': True,
                    'price': price,
                    'error_message': False,
                    'warning_message': False}


class FixedShippingCostRange(models.Model):
    _name = "fixed.shipping.cost.range"
    _description = "Fixed Shipping Cost Range"

    weight_start_range = fields.Float(string="Weight From")
    weight_end_range = fields.Float(string="Weight To")
    price = fields.Float(string="Shipping Cost")
    delivery_carrier_id = fields.Many2one('delivery.carrier', string="Delivery Method")