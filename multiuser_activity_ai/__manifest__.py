# -*- coding: utf-8 -*-

{
    'name': 'Multi Users Activity',
    'version': '1.0',
    'summary': """This module allows you to assign activity to the multiple users and all users receive live notification
     if action schedule for current day.
    """,
        'license': 'AGPL-3',
    "description": """
        Assign Activity to the multiple users
        Multi user activity,
        assign activity more than one users.
    """,
    'author': 'Anil(anil.patel13688@gmail.com)',
    'company': 'Valueble IT Solution',
    'depends': ['base', 'mail', 'web'],
    'data': [
        'views/mail_activity.xml'
    ],
    'price': "50",
    'website': 'valuebleitsolution.odoo.com',
    'currency': 'USD',
    'author': 'Valueble IT Solution',
    'images': ['static/description/img_3.png'],
    'qweb': ['static/src/xml/activity.xml',],
    'demo': [],
    'installable': True,
    'application': True,
}
