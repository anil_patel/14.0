# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class MailActivity(models.Model):
    _inherit = 'mail.activity'

    additional_user_ids = fields.Many2many('res.users', 'rel_multi_user_activity', string="Additional Users")
    multi_users = fields.Char(compute="get_multiuser_data")

    def get_multiuser_data(self):
        for activity in self:
            user_lst = []
            if activity.additional_user_ids:
                if activity.user_id:
                    user_lst.append('1) ' + activity.user_id.name)
                cnt = 2 if activity.user_id else 1
                for user in activity.additional_user_ids:
                    if not activity.user_id or (activity.user_id != user):
                        user_lst.append(str(cnt) + ') ' + user.name)
                        cnt = cnt + 1
            else:
                user_lst.append(activity.user_id.name)
            activity.multi_users = ', '.join(user_lst)

    def action_notify_multiuser(self):
        if not self:
            return
        original_context = self.env.context
        body_template = self.env.ref('mail.message_activity_assigned')
        for activity in self:
            if activity.additional_user_ids:
                for user_id in activity.additional_user_ids:
                    if user_id != activity.user_id:
                        if user_id.lang:
                            # Send the notification in the assigned user's language
                            self = self.with_context(lang=user_id.lang)
                            body_template = body_template.with_context(lang=user_id.lang)
                            activity = activity.with_context(lang=user_id.lang)
                        model_description = self.env['ir.model']._get(activity.res_model).display_name
                        body = body_template._render(
                            dict(
                                activity=activity,
                                model_description=model_description,
                                access_link=self.env['mail.thread']._notify_get_action_link('view', model=activity.res_model, res_id=activity.res_id),
                            ),
                            engine='ir.qweb',
                            minimal_qcontext=True
                        )
                        record = self.env[activity.res_model].browse(activity.res_id)
                        record.message_notify(
                            partner_ids=user_id.partner_id.ids,
                            body=body,
                            subject=_('%(activity_name)s: %(summary)s assigned to you',
                                activity_name=activity.res_name,
                                summary=activity.summary or activity.activity_type_id.name),
                            record_name=activity.res_name,
                            model_description=model_description,
                            email_layout_xmlid='mail.mail_notification_light',
                        )
                        body_template = body_template.with_context(original_context)
                        self = self.with_context(original_context)
                        self.env[activity.res_model].browse(activity.res_id).message_subscribe(partner_ids=[user_id.partner_id.id])
                        if activity.date_deadline <= fields.Date.today():
                            self.env['bus.bus'].sendone(
                                (self._cr.dbname, 'res.partner', user_id.partner_id.id),
                                {'type': 'activity_updated', 'activity_created': True})

    @api.model
    def create(self, vals):
        res = super(MailActivity, self).create(vals)
        if res.additional_user_ids:
            res.action_notify_multiuser()
        return res

    def write(self, vals):
        res = super(MailActivity, self).write(vals)
        if 'additional_user_ids' in vals and self.additional_user_ids:
            self.action_notify_multiuser()
        return res


class MailActivityMixin(models.AbstractModel):
    _inherit = 'mail.activity.mixin'

    def _search_my_activity_date_deadline(self, operator, operand):
        activity_ids = self.env['mail.activity']._search([
            ('date_deadline', operator, operand),
            ('res_model', '=', self._name),
            '|',
            ('additional_user_ids', 'in', [self.env.user.id]),
            ('user_id', '=', self.env.user.id)
        ])
        return [('activity_ids', 'in', activity_ids)]