# -*- coding: utf-8 -*-

from odoo import _, api, exceptions, fields, models, modules
from odoo.addons.base.models.res_users import is_selection_groups


class Users(models.Model):
    _inherit = 'res.users'

    @api.model
    def get_activity_domain(self, model):
        activity_ids = self.env['mail.activity'].sudo().search([('res_model', '=', model),'|',('user_id', '=', self.env.user.id), ('additional_user_ids', 'in', [self.env.user.id])])
        if activity_ids:
            return [['activity_ids.id', 'in', activity_ids.ids]]
        return False

    @api.model
    def systray_get_activities(self):
        activities_res = super(Users, self).systray_get_activities()
        query = """SELECT m.id, count(*), act.res_model as model,
                                CASE
                                    WHEN %(today)s::date - act.date_deadline::date = 0 Then 'today'
                                    WHEN %(today)s::date - act.date_deadline::date > 0 Then 'overdue'
                                    WHEN %(today)s::date - act.date_deadline::date < 0 Then 'planned'
                                END AS states
                            FROM mail_activity AS act
                            LEFT JOIN rel_multi_user_activity as mu on act.id = mu.mail_activity_id
                            JOIN ir_model AS m ON act.res_model_id = m.id
                            WHERE mu.res_users_id = %(user_id)s AND user_id != %(user_id)s
                            GROUP BY m.id, states, act.res_model;
                            """

        self.env.cr.execute(query, {
            'today': fields.Date.context_today(self),
            'user_id': self.env.uid,
            'user_id': self.env.uid,
        })
        activity_data = self.env.cr.dictfetchall()
        model_ids = [a['id'] for a in activity_data]
        model_names = {n[0]: n[1] for n in self.env['ir.model'].browse(model_ids).name_get()}

        user_activities = {}
        for activity in activity_data:
            flag = False
            temp_dict = False
            if activities_res:
                for each in activities_res:
                    if 'model' in each and each.get('model') == activity['model']:
                        flag = True
                        temp_dict = each
                        break
            if not user_activities.get(activity['model']) and not flag:
                module = self.env[activity['model']]._original_module
                icon = module and modules.module.get_module_icon(module)
                user_activities[activity['model']] = {
                    'name': model_names[activity['id']],
                    'model': activity['model'],
                    'type': 'activity',
                    'icon': icon,
                    'total_count': 0, 'today_count': 0, 'overdue_count': 0, 'planned_count': 0,
                }
            if flag:
                temp_dict['%s_count' % activity['states']] += activity['count']
                if activity['states'] in ('today', 'overdue'):
                    temp_dict['total_count'] += activity['count']

                temp_dict['actions'] = [{
                    'icon': 'fa-clock-o',
                    'name': 'Summary',
                }]
            else:
                user_activities[activity['model']]['%s_count' % activity['states']] += activity['count']
                if activity['states'] in ('today', 'overdue'):
                    user_activities[activity['model']]['total_count'] += activity['count']

                user_activities[activity['model']]['actions'] = [{
                    'icon': 'fa-clock-o',
                    'name': 'Summary',
                }]
        if user_activities:
            return activities_res + list(user_activities.values())
        return activities_res

