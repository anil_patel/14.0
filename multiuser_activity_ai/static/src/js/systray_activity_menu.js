odoo.define('multiuser_activity_ai.systray.ActivityMenu', function (require) {
"use strict";
var core = require('web.core');
var session = require('web.session');
var SystrayMenu = require('web.SystrayMenu');
var Widget = require('web.Widget');
var Time = require('web.time');
var QWeb = core.qweb;

const { Component } = owl;
var ActivityMenu = require('mail.systray.ActivityMenu');

ActivityMenu.include({
    //--------------------------------------------------
    // Private
    //--------------------------------------------------
    /**
     * @override
     */

    _onActivityActionClick: function (ev) {
        ev.stopPropagation();
        this.$('.dropdown-toggle').dropdown('toggle');
        var targetAction = $(ev.currentTarget);
        var actionXmlid = targetAction.data('action_xmlid');
        if (actionXmlid) {
            this.do_action(actionXmlid);
        } else {
            var domain = [['activity_ids.user_id', '=', session.uid]]
            if (targetAction.data('domain')) {
                domain = domain.concat(targetAction.data('domain'))
            }
            var $self = this;

            this._rpc({
            model: 'res.users',
            method: 'get_activity_domain',
            args: [targetAction.data('res_model')]
            }).then(function (result) {
                if (result){
                    $self.do_action({
                        type: 'ir.actions.act_window',
                        name: targetAction.data('model_name'),
                        views: [[false, 'activity'], [false, 'kanban'], [false, 'list'], [false, 'form']],
                        view_mode: 'activity',
                        res_model: targetAction.data('res_model'),
                        domain: result,
                        }, {
                            clear_breadcrumbs: true,
                        });
                }
                else{
                     $self.do_action({
                        type: 'ir.actions.act_window',
                        name: targetAction.data('model_name'),
                        views: [[false, 'activity'], [false, 'kanban'], [false, 'list'], [false, 'form']],
                        view_mode: 'activity',
                        res_model: targetAction.data('res_model'),
                        domain: domain,
                        }, {
                            clear_breadcrumbs: true,
                        });
                }
            });
        }
    },


    _onActivityFilterClick: function (event) {
        // fetch the data from the button otherwise fetch the ones from the parent (.o_mail_preview).
        var data = _.extend({}, $(event.currentTarget).data(), $(event.target).data());
        var context = {};
        if (data.filter === 'my') {
            context['search_default_activities_overdue'] = 1;
            context['search_default_activities_today'] = 1;
        } else {
            context['search_default_activities_' + data.filter] = 1;
        }
        // Necessary because activity_ids of mail.activity.mixin has auto_join
        // So, duplicates are faking the count and "Load more" doesn't show up
        context['force_search_count'] = 1;

        var domain = [['activity_ids.user_id', '=', session.uid]]
        if (data.domain) {
            domain = domain.concat(data.domain)
        }
        var $self = this;
        this._rpc({
        model: 'res.users',
        method: 'get_activity_domain',
        args: [data.res_model]
        }).then(function (result) {
            if (result){
                $self.do_action({
                    type: 'ir.actions.act_window',
                    name: data.model_name,
                    res_model: data.res_model,
                    views: $self._getViewsList(data.res_model),
                    search_view_id: [false],
                    domain: result,
                    context:context,
                },
                {
                    clear_breadcrumbs: true,
                });
            }
            else{
                $self.do_action({
                    type: 'ir.actions.act_window',
                    name: data.model_name,
                    res_model: data.res_model,
                    views: $self._getViewsList(data.res_model),
                    search_view_id: [false],
                    domain: domain,
                    context:context,
                },
                {
                    clear_breadcrumbs: true,
                });
            }
        });

    },

});
});
