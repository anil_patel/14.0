odoo.define('multiuser_activity_ai/static/src/models/activity/activity.js', function (require) {
'use strict';

const {
    registerClassPatchModel,
    registerFieldPatchModel,
    registerInstancePatchModel,
} = require('mail/static/src/model/model_core.js');
const { attr } = require('mail/static/src/model/model_field.js');

registerClassPatchModel('mail.activity', 'multiuser_activity_ai/static/src/models/activity/activity.js', {
    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    /**
     * @override
     */
    convertData(data) {
        const data2 = this._super(data);
        if ('multi_users' in data) {
            data2.multi_users = data.multi_users;
        }
        return data2;
    },
});

registerFieldPatchModel('mail.activity', 'voip/static/src/models/activity/activity.js', {
    multi_users: attr(),
});

});
