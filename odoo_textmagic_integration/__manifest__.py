# -*- coding: utf-8 -*-

{
    'name': 'Textmagic Odoo Integration',
    'version': '1.0',
    'summary': 'This module allows you to send sms bulk sms from contact module using textmagic sms gateway.',
    'license': 'AGPL-3',
    'description': '''
    Textmagic odoo
    textmagic odoo sms
    textmagic integration odoo
    textmagic sms gateway
    Textmagic odoo integration
    odoo textmagic
    ''',
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    'depends': ['base', 'contacts'],
    "data": [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/textmagic.xml',
        'wizard/send_sms.xml'
        ],
    'price': "10",
    'website': 'valuebleitsolution.odoo.com',
    'currency': 'USD',
    'images': ['static/description/img_1.png'],
    'demo': [],
    'installable': True,
    'application': True,
}
