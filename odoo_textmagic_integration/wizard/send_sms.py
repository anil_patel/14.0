# -*- coding: utf-8 -*-

from datetime import date,timedelta,datetime

from odoo import api, models, fields,_
from odoo.exceptions import UserError, ValidationError
import logging
import sys
import traceback
import time # Import whole time module
import TextMagic
from TextMagic.rest import ApiException
from datetime import date


class TextmagicWiz(models.TransientModel):
    _name = 'textmagic.wizard'
    _description = "textmagic wizard"

    sms = fields.Text(string="SMS")

    def confirm(self):
        config_id = self.env['textmagic.gateway'].search([], limit=1)
        if not config_id:
            raise ValidationError('Please set textmagic configuration in settings.')
        if (config_id and not config_id.username) or (config_id and not config_id.password):
            raise ValidationError('Please set username and password for textmagic')
        log_id = self.env['textmagic.logs'].create({'date':date.today(), 'sms': self.sms})
        partner_ids = self.env['res.partner'].sudo().browse(self._context.get('active_ids'))
        sms = self.sms
        for partner in partner_ids:
            if partner.phone:
                configuration = TextMagic.Configuration()
                configuration.username = config_id.username
                configuration.password = config_id.password
                api_instance = TextMagic.TextMagicApi(TextMagic.ApiClient(configuration))
                try:
                    send_message_input_object = TextMagic.SendMessageInputObject()
                    send_message_input_object.text = sms
                    phone = partner.phone
                    send_message_input_object.phones = phone
                    response = api_instance.send_message(send_message_input_object)
                    self.env['textmagic.logs.line'].create({'partner_id': partner.id, 'result': 'success', 'log_id':log_id.id})
                except ApiException as e:
                    print("Exception when sending textmagic sms: %s\n" % e)
                    self.env['textmagic.logs.line'].create({'partner_id': partner.id, 'result': e, 'log_id': log_id.id})
        action = self.env["ir.actions.actions"]._for_xml_id("odoo_textmagic_integration.action_textmagic_logs")
        action['domain'] = [('id', '=', log_id.id)]
        return action