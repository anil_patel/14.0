# -*- coding: utf-8 -*-

{
    'name': 'Portal Quotation Followup',
    'version': '1.0',
    'summary': """ This module create the followup activity for the salesperson when customer open quotation from portal and customer signd the quotation from portal.
""",
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    "description": """
        Quotatioin Followup.
        quotation followup.
        create activity on quotatoin signed.
        salesperson activity,
        notify salesperson on quotation signed.
        log notes search
    """,
    'price': "20",
    'currency': 'USD',
    'license': 'AGPL-3',
    'website': 'valuebleitsolution.odoo.com',
    'images': ['static/description/img_5.png'],
    'depends': ['mail','sale'],
    'data': [

    ],
    'demo': [],
    'installable': True,
    'application': True,
}