# -*- coding: utf-8 -*-

{
    'name': 'Search Notes',
    'version': '1.0',
    'summary': """This module allow the search sale order, customer invoice, purchase order, vendor bill, delivery order and transfer based on the log notes.
""",
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    "description": """
    search notes.
    advance search.
    search log notes.
    search order notes,
    log notes search
""",
    'price': "15",
    'currency': 'USD',
    'license': 'AGPL-3',
    'website': 'valuebleitsolution.odoo.com',
    'images': ['static/description/img_2.png'],
    'depends': ['base', 'sale', 'account', 'purchase', 'stock'],
    'data': [
        'view/sale_search_view.xml',
        'view/invoice_search_view.xml',
        'view/purchase.xml',
        'view/stock_picking.xml'
    ],
    'demo': [],
    'installable': True,
    'application': True,
}