# -*- coding: utf-8 -*-

{
    'name': 'Survey Reminder',
    'version': '1.0',
    'summary': 'this module allow to send auto reminder for survey based on reminder days.',
    'license': 'AGPL-3',
    'description': '''
    Survey Reminder,
    survey reminder,
    auto reminder survey,
    reminder for survey,
    automated reminder for survey,
    ''',
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    'depends': ['base', 'survey'],
    "data": [
        'data/mail_template.xml',
        'data/ir_cron.xml',
        'views/survey.xml',
        ],
    'price': "30",
    'website': 'valuebleitsolution.odoo.com',
    'currency': 'USD',
    'images': ['static/description/img_4.png'],
    'demo': [],
    'installable': True,
    'application': True,
}
