# -*- coding: utf-8 -*-

from datetime import date,timedelta,datetime

from odoo import api, models, fields,_
from odoo.exceptions import UserError, ValidationError
import logging
import sys
import traceback
import time # Import whole time module


_logger = logging.getLogger(__name__)

class SurveySurvey(models.Model):
	_inherit = 'survey.survey'

	reminder_day = fields.Integer(string="Reminder After Days", tracking=True)
	has_input_ids = fields.Boolean(string="", compute="check_user_input_ids")

	def check_user_input_ids(self):
		self.has_input_ids = False
		if self.user_input_ids:
			self.has_input_ids = True

	@api.model
	def send_reminder_for_survey(self):
		survey_ids = self.search([('state', '=', 'open'),('reminder_day','>',0)])
		if survey_ids:
			for survey in survey_ids:
				date_search = date.today() - timedelta(days=survey.reminder_day)
				input_ids = self.env['survey.user_input'].search(
					[('state', '!=', 'done'), ('survey_id', '=', survey.id), ('date_open', '=', date_search)])
				template_id = self.env.ref('survey_reminder.mail_template_user_input_invite_cron_reminder')
				if input_ids:
					for invite in input_ids:
						if invite.email:
							try:
								template_id.with_context(notif_layout='mail.mail_notification_light').send_mail(invite.id,notif_layout="mail.mail_notification_light")
								invite.date_open = date.today()
							except Exception:
								invite.date_open = date.today()


class SurveyUserInput(models.Model):
	""" Metadata for a set of one user's answers to a particular survey """
	_inherit = "survey.user_input"

	date_open = fields.Date(string="Date Open")

	@api.model
	def create(self, vals):
		res = super(SurveyUserInput, self).create(vals)
		res.date_open = res.create_date.date()
		return res