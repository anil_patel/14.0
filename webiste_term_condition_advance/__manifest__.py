# -*- coding: utf-8 -*-
{
    'name': 'Website Terms & Conditions Advance',
    'version': '1.0',
    'category': 'Website',
    'summary': 'This module allows to use different terms and condition for different product category.',
    'description': '''
    Term&Conditions advance
    product category terms and condition
    allow to use term condition product wise
    website term and conditions advance
    allow more than one term and condition.
    product category wise terms and conditions
    ''',
    'author': 'Valueble IT Solution',
    'company': 'Valueble IT Solution',
    'price': "30",
    'currency': 'USD',
    'depends': ['sale_management', 'sale', 'website','website_sale_delivery'],
    "data": [
        "security/ir.model.access.csv",
        "views/assets.xml",
        "views/website.xml",
        "views/address_template.xml",
        'views/term_template.xml',
       
        ],
    'images': ['static/description/img_9.png'],
    'application': True,
    'installable': True,
    'auto_install': False,
    'license': 'OPL-1',
    'external_dependencies': {
    },
}
