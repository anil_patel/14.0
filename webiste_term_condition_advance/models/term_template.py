# -*- coding: utf-8 -*-

from odoo import fields, models, api


class PosTermTemplate(models.Model):
    _name = 'pos.term.template'
    _description = 'POS Term Condition Template'

    name = fields.Char(string="Name")
    term = fields.Html(string="Term and Condition")
    product_ids = fields.Many2many('product.product', string="Products")


class ProductProduct(models.Model):
    _inherit = 'product.product'

    website_term = fields.Html(string="Term & Conditions")

class Website(models.Model):
    _inherit = 'website'

    terms_and_conditions = fields.Html(string='Description')
