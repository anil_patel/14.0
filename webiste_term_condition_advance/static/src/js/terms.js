odoo.define('website_terms_and_condition.terms', function (require) {
"use strict";
    var ajax = require('web.ajax');
    var core = require('web.core');
    var PaymentForm = require('payment.payment_form');

    var _t = core._t;

    PaymentForm.include({
        payEvent: function (ev) {
            var term_accept = true;
            if ($(".term_chk").length > 0){
              $(".term_chk").each(function(){
                if (this.checked == false){
                    term_accept = false;
                }
              })
            }
            var self = this;
            if (ev.type === 'submit') {
                var button = $(ev.target).find('*[type="submit"]')[0]
            } else {
                var button = ev.target;
            }

            if (term_accept == false){
                alert("Please Accept our Teams and Conditions By clicking Read More...")
                self.enableButton(button);
                return false;
            }
            else if ($('#order_type_pay').val() && $('#order_type_pay').val() == 'add_ons'){
                return this._super.apply(this, arguments);
            }

            else{
                return this._super.apply(this, arguments);
            }
        },
    });

    $('.read_more_btn').on('click', function(){
            var tmpl_id = this.value
            ajax.jsonRpc("/get/term/template", 'call', {'template_id': this.value})
            .then(function (data){
              if (data){
                   $('#term_name').text(data['name']);
                   $('#term_condition_datails').html(data['term']);
                   $('#term_template_id').val(tmpl_id);
                   $('#terms_condition_modal').modal('show');
              }
        });
    });


    $('#check_pos_term').on('click', function(){
        var term_accept = true;
        if ($(".term_chk").length > 0){
          $(".term_chk").each(function(){
            if (this.checked == false){
                term_accept = false;
            }
          })
        }
        if (term_accept == false){
            alert('Please accept/agree to POS Agreements by clicking Read More...');
        }


        else{
            $('#modalaccept').modal('show');
        }
    });

    $('#chatter_pos_term').on('click', function(){
        var term_accept = true;
        if ($(".term_chk").length > 0){
          $(".term_chk").each(function(){
            if (this.checked == false){
                term_accept = false;
            }
          })
        }
        if (term_accept == false){
            alert('Please accept/agree to POS Agreements by clicking Read More...');
        }

        else{
            $('#modalaccept').modal('show');
        }
    });

//    $('#delivery_carrier .o_delivery_carrier_select').on('click', function(){
//        $('#delivery_warning_sign').hide();
//    });

    $('.signup_country').on('change', function(){
        var country_id = $('.signup_country').val();
        if (country_id){
            ajax.jsonRpc("/get/signup/states/", 'call', {'country_id': country_id})
            .then(function (data){
              if (data){
                $('.signup_state').empty();
                $('.signup_state').append(new Option('Select State', ''));
                for (const key of Object.keys(data)){
                    $('.signup_state').append(new Option(data[key], key));
                }
              }
              else{
                $('.signup_state').empty();
                $('.signup_state').append(new Option('', 'Select State'));
              }
            });
        }
        else{
            console.log('\n\n no country selecte')
        }
    });

    $('#popup_accept_term_pos').on('click', function(){
        if ($('#pos_term_chk').is(":checked")){
            $(".term_chk").each(function(){
                if (this.value == $('#term_template_id').val()){
                    this.checked = 'checked';
                }
              })
            ajax.jsonRpc("/accept/pos/term/", 'call', {'sale_id': $('#sale_id_hidden').val(),'accept_date': $('#pos_term_accept_date').val(), template_id:$('#term_template_id').val()})
            .then(function (data){
                $('#terms_condition_modal').modal('hide');
            });    
        }
        else{
            alert('Please accept the Agreements.');
        }
    });

    var date = new Date();
    $('#pos_term_accept_date').datepicker({
        dateFormat:"mm-dd-yy"
    });

    $('#pos_term_chk').on('change', function(){
        if($('#pos_term_chk').is(":checked")){
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            $('#pos_term_accept_date').val(month + '-' + day + '-' + year);    
        }
        else{
            $('#pos_term_accept_date').val('');
        }  
    });



});
