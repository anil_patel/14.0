# -*- coding: utf-8 -*-

{
    "name": "Website Multi Promotion",
    "author": "Valueble IT Solution",
    "support": "anil.patel13688@gmail.com",
    'website': 'valuebleitsolution.odoo.com',
    "category": "Warehouse",
    "summary": """This module allow you to apply a multiple promo code on website""",
    "description": """
    Apply Multiple Promo Code,
    Multiple Promo Code,
    Website Multiple Promo code,
    promo code,
    multiple coupon code,
    website_multi_coupon,
    apply multiple coupon code,
    """,
    "version": "14.0.1",
    'license': 'AGPL-3',
    'company': 'Valueble IT Solution',
    'depends': ['base', 'sale_coupon'],
    'images': ['static/description/img1.png'],
    'data': [
    ],
    'demo': [],
    'installable': True,
    'application': True,
    "price": "15",
    "currency": "EUR"
}
